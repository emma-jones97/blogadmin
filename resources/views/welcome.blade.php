@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome</div>

                <div class="panel-body">
                    Welcome to the survey creator site.
                </div>

                @if(Auth::check())
                    <div class="panel-body">
                        Hello {{ Auth::user()->name }}, here you can create questionnaires and complete questionnaires.
                    </div>
                    <div class="panel-body">
                        <b>Go to <a href="/home">Home</a> to get started.<b>
                    </div>
                @else
                <div style="text-align:center" class="panel-body">
                   Sign in to access all features<br>
                    <button style="margin:10px;" type="button"><a href="/login">Login</a></button><button type="button"><a href="/register">Sign up</a></button>
                </div>
                    @endif

            </div>
        </div>
    </div>
</div>
@endsection
