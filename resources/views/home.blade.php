@extends('layouts.app')
@section('content')
    <!-- WELCOME BOX INTRODUCING USER TO SITE -->
    <div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div style="font-size: large;"  class="panel-body">
                    Hello {{ Auth::user()->name }}, Welcome to the Home page!
                </div>
                <!-- BUTTONS TO HELP USER GET STARTED (either create or answer surveys) -->
                <div style="text-align: center; font-size: large;" class="panel-body">
                    <b>Get Started:</b><br>
                    <button style="margin:10px;" type="button">Create Questionnaire</button><button type="button">Answer Questionnaire</button>
                    </div>

            </div>


            <!--SEPARATE CONTAINER TO SHOW USERS RECENT SURVEYS AND OTHER SURVEY INTERACTIONS -->
            <div class="panel panel-default">
                <div class="panel-heading">My Surveys</div>

                <div style="font-size: large;"  class="panel-body">
                    Hello {{ Auth::user()->name }}, here you can find your questionnaires you have created.

                        <h2>Recent Surveys
                            <button><a href="/survey/new">Create new</a></button>
                        </h2>

                </div>



            </div>

        </div>
    </div>
</div>


    @endsection

