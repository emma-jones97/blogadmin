<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Surveys</title>
</head>
<body>
<h1>Surveys</h1>



<section>
    @if (isset ($surveys))

        <ul>
            @foreach ($surveys as $survey)
                <li><a href="surveys/view/{{ $survey->id }}" name="{{ $survey->title }}">{{ $survey->title }}</a></li>
            @endforeach
        </ul>
    @else
        <p> no surveys added yet </p>
    @endif
</section>

{{ Form::open(array('action' => 'SurveyController@create', 'method' => 'get')) }}
<div class="row">
    {!! Form::submit('Add Survey', ['class' => 'button']) !!}
</div>
{{ Form::close() }}

</body>
</html>