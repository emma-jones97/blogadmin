@extends('layouts.app')

<html lang="en">
<head>
    <title>Questionnaire</title>
</head>
<body>
@section('content')
    <h1>Questionnaire</h1>
    <form style="padding: 2%" action="surveys.blade.php" name="user_id" method="post">
        First name: <input type="text" name="firstname"><br><br>
        Last name: <input type="text" name="lastname"><br><br>

        Gender:<br>
        <input type="checkbox" name="sex" value="male">Male<br><br>
        <input type="checkbox" name="sex" value="female">Female<br><br>
        <input type="checkbox" name="sex" value="na">Prefer not to say<br><br>

        Age:<br>
        <input type="radio" name="age" value="1-17">1-17<br><br>
        <input type="radio" name="age" value="18-25">18-25<br><br>
        <input type="radio" name="age" value="26-30">26-30<br><br>
        <input type="radio" name="age" value="31-40">31-40<br><br>
        <input type="radio" name="age" value="41+">41+<br><br>

        <input type="submit" value="submit questionnaire"/>
    </form>
    @endsection
</body>
</html>