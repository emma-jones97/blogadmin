<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Article;


class OpenArticleController extends Controller
{
    /*
     * Get all articles and return them as a json collection.
     */
    public function index()
    {
        // get all the articles
        $articles = Article::with('user', 'categories')->get(); // should not do this on frontend.


        //return $articles; // step one seeing the data returned as json with no collection name.
        return response()->json(['data' => $articles], 200); // step two return the collection with the name of data
    }


    public function show($id)
    {


        $article = Article::with('user', 'categories')->where('id', $id)->first();

        //return $article; // step 1 shows all data with linked data as sub-collections.

        //step 2 capture exceptions and or return data.

        // if article does not exist return to list
        if(!$article) {
            return response()->json([
                'error' => ['message' => 'Article does not exist']
            ], 404);
        }

        // if article exists clean it via the transform method.
        return response()->json([
            'data' => $this->transform($article)
        ], 200);
    }
    /*
  * Take the collection and separate out as individual articles to pass through the transform method.
  */
    private function transformCollection($articles){
        return array_map([$this, 'transform'], $articles->toArray());
    }


    /*
     * transform AN article to restrict fields and change the true DB column names.
     *
     * article author shows how we access joined tables data.
     */
    private function transform($article){
        return [
            'article_id' => $article['id'],
            'article_title' => $article['title'],
            'article_body' => $article['content'],
            'article_author' => $article['user']['name'],
            //'article_author' => $article->user->name,   this is an alternative method to achieve the same as line above.
        ];
    }


}