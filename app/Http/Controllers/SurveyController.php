<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Surveys;

use App\Category;

use Auth;

class SurveyController extends Controller
{

/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/

/*
* Secure the set of pages to the admin.
*/
public function __construct()
{
$this->middleware('auth');
}

    public function index()
    {
        // get all the articles
        $surveys = Surveys::all();

        return view('survey/view', ['surveys' => $surveys]);
    }


    public function show($id)
    {
        return view('');

    }

    public function create()
    {
        $cats = Category::lists('title', 'id');

        // now we can return the data with the view
        return view('survey/surveys/create', compact('cats'));

    }
}