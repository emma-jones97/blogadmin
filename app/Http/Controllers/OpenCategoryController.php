<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Article;


class OpenCategoryController extends Controller
{
    /*
     * Get all articles and return them as a json collection.
     */
    public function index()
    {
        // get all the categories
        $category = Category::with('user', 'categories')->get(); // should not do this on frontend.


        //return $categories; // step one seeing the data returned as json with no collection name.
        //return response()->json(['data' => $articles], 200); // step two return the collection with the name of data
        return response()->json([
            'data' => $this->transformCollection($category)
        ], 200); // step 3 return the collection to the transformCollection method to clean it up and add basic security.
    }


    public function show($id)
    {


        $category = Category::with('user', 'categories')->where('id', $id)->first();

        //return $category; // step 1 shows all data with linked data as sub-collections.

        //step 2 capture exceptions and or return data.

        // if category does not exist return to list
        if(!$category) {
            return response()->json([
                'error' => ['message' => 'Category does not exist']
            ], 404);
        }

        // if article exists clean it via the transform method.
        return response()->json([
            'data' => $this->transform($category)
        ], 200);
    }
    /*
  * Take the collection and separate out as individual articles to pass through the transform method.
  */
    private function transformCollection($category){
        return array_map([$this, 'transform'], $category->toArray());
    }


    /*
     * transform AN article to restrict fields and change the true DB column names.
     *
     * article author shows how we access joined tables data.
     */
    private function transform($category){
        return [
            'category_id' => $category['id'],
            'category_title' => $category['title'],
            'category_body' => $category['content'],
            'category_author' => $category['user']['name'],
            //'category_author' => $category->user->name,   this is an alternative method to achieve the same as line above.
        ];
    }


