var categoryApp = angular.module('categoryApp', ['ngRoute', 'CategoryControllers', 'categoryService']); //'ngRoute', 'CategoryControllers',

// create the routes for the angular build. Are called when http://localhost:8000/app/index.html is called
categoryApp.config(['$routeProvider',
    function($routeProvider) {
    $routeProvider.
        // all category articles in list
        when('/category', {
            templateUrl: 'scripts/views/list.html',
            controller: 'categoryController'
        }).
        // individual category
        when('/category/:id', {
            templateUrl: 'scripts/views/category.html',
            controller: 'articleController'
        }).

        // auto forward to http://localhost:8000/app/index.html
        otherwise({
            redirectTo: '/category'
    });
    }]);