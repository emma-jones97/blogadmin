angular.module('blogService', [])

.factory('Blog', funtion($http), {
    return:{
        get : function() {
            return $http.get('/api/v1/articles');
        }
    }
});