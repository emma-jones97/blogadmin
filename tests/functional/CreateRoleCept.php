<?php
$I = new FunctionalTester($scenario);

$I->am('admin');
$I->wantTo('create a new role');

// When
$I->amOnPage('/admin/roles');
$I->see('Roles', 'h1');
$I->dontSee('Dave Walsh');
// And
$I->click('Add Role');

// Then
$I->amOnPage('/admin/roles/create');
// And
$I->see('Add Role', 'h1');
$I->submitForm('#createrole', [
    'name' => 'Dave Walsh',
    'email' => 'walshd@edgehill.ac.uk',
    'password' => 'password'
]);
// Then
$I->seeCurrentUrlEquals('/admin/role');
$I->see('role', 'h1');
$I->see('New role added!');
$I->see('Dave Walsh');



// Check for duplicates

// When
$I->amOnPage('/admin/roles');
$I->see('Roles', 'h1');
$I->see('Dave Walsh');
// And
$I->click('Add Role');

// Then
$I->amOnPage('/admin/roles/create');
// And
$I->see('Add Role', 'h1');
$I->submitForm('#createrole', [
    'name' => 'Dave Walsh',
    'email' => 'walshd@edgehill.ac.uk',
    'password' => 'password'
]);
// Then
$I->seeCurrentUrlEquals('/admin/roles');
$I->see('Roles', 'h1');
$I->see('Error role already exists!');
