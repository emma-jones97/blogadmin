-- MySQL dump 10.13  Distrib 5.7.17, for Linux (x86_64)
--
-- Host: localhost    Database: blogadmin
-- ------------------------------------------------------
-- Server version	5.7.17-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `article_category`
--

DROP TABLE IF EXISTS `article_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article_category` (
  `article_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  KEY `article_category_article_id_index` (`article_id`),
  KEY `article_category_category_id_index` (`category_id`),
  CONSTRAINT `article_category_article_id_foreign` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `article_category_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article_category`
--

LOCK TABLES `article_category` WRITE;
/*!40000 ALTER TABLE `article_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `article_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `articles`
--

DROP TABLE IF EXISTS `articles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `articles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `published_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `articles_title_unique` (`title`),
  KEY `articles_published_at_index` (`published_at`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `articles`
--

LOCK TABLES `articles` WRITE;
/*!40000 ALTER TABLE `articles` DISABLE KEYS */;
INSERT INTO `articles` VALUES (1,'Consequatur consequatur molestiae officiis sapiente et.','Unde commodi dolorem sint. Exercitationem a atque sit aut. Deleniti quibusdam veniam explicabo sint beatae asperiores. Ex doloremque sed doloribus. Sed ea eos perferendis eum facilis qui et.','beatae',1,'2017-03-06 21:28:48','2017-03-06 21:28:48','0000-00-00 00:00:00'),(2,'Libero id qui nisi commodi iure.','Adipisci ad ea libero nihil aut. Suscipit eius aliquam ipsa non architecto nam. Id quos aut quis quas.','consequatur',1,'2017-03-06 21:28:48','2017-03-06 21:28:48','0000-00-00 00:00:00'),(3,'Qui eius alias mollitia laudantium dignissimos unde.','Quaerat et quis laboriosam sunt temporibus possimus consequatur. Facilis praesentium iure odio autem quibusdam. Et qui perspiciatis repellat id est sunt rerum. Quia minima unde maxime ut consequuntur sint aut. Quidem mollitia mollitia nesciunt est qui.','et',1,'2017-03-06 21:28:48','2017-03-06 21:28:48','0000-00-00 00:00:00'),(4,'Laborum molestias similique in qui incidunt rerum.','Labore maiores adipisci eos nobis. Illum delectus ad aut et iste repudiandae. Ea error labore nisi quis voluptatem omnis. Veniam aperiam qui repellat quia.','dolorum',1,'2017-03-06 21:28:48','2017-03-06 21:28:48','0000-00-00 00:00:00'),(5,'Reiciendis dicta excepturi atque eaque.','Provident perspiciatis assumenda tenetur aut autem blanditiis quibusdam. Qui et aliquam veritatis ipsum consequatur dolore. Et harum iusto laudantium laboriosam quia. Et excepturi est similique sint.','ut',1,'2017-03-06 21:28:48','2017-03-06 21:28:48','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `articles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `detail` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_title_unique` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'HTML','html',NULL,NULL),(2,'PHP','php',NULL,NULL),(3,'CSS','css',NULL,NULL);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2014_10_12_000000_create_users_table',1),('2014_10_12_100000_create_password_resets_table',1),('2016_02_19_100050_create_roles_table',1),('2016_02_19_100108_create_permissions_table',1),('2016_02_19_100123_create_articles_table',1),('2016_02_19_100137_create_categories_table',1),('2016_02_19_100200_create_role_user_table',1),('2016_02_19_100219_create_permission_role_table',1),('2016_02_19_100236_create_article_category_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_user` (
  `role_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`user_id`),
  KEY `role_user_user_id_foreign` (`user_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_user`
--

LOCK TABLES `role_user` WRITE;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'emma','emma@example.com','$2y$10$Fx2jySpxIjND1Np1iV5zMuXYNsI1czZSfBtZLUCSJk07scHWZIqAO','Nv093bObDd',NULL,NULL),(2,'Eula Lindgren','egleichner@example.org','$2y$10$Lpbg6OV7x5txH0JMpj3y3.fUMalfp6EGIbWM9Ut8EFW9XcScxloqC','Nt0vBRPA1W','2017-03-06 21:28:47','2017-03-06 21:28:47'),(3,'Prof. Ignacio Johnson Jr.','hartmann.keith@example.org','$2y$10$mJHwm1.2pWIDLX8KELPDh.1KYjkWSWLJv3ampjthgyi2mRMzir3tq','OvrGvUMXBb','2017-03-06 21:28:47','2017-03-06 21:28:47'),(4,'Gianni Trantow Sr.','koby.schuster@example.net','$2y$10$h.uk3eTdhtCtETGXJq0j/O9hOB8s79vE/KdyyOXV3HdILWhoxCq4W','7e8DVskrpA','2017-03-06 21:28:47','2017-03-06 21:28:47'),(5,'Mr. Tyree Upton Sr.','opal.bartoletti@example.net','$2y$10$pVTXkRjKpJziHrWuVfGFJ.qJraXP4MYj8c9M2yt5624dMq73/ffVO','20fFLsVEyE','2017-03-06 21:28:47','2017-03-06 21:28:47'),(6,'Sandrine Deckow','hollie12@example.com','$2y$10$fm5MU5Az03Gf8XIgNQIqLOnCLo8OuGTXsw1Z6XV0e6mEpxHlmBCLe','Vj4tbiErvt','2017-03-06 21:28:47','2017-03-06 21:28:47'),(7,'Mr. Wilton Abshire MD','wolff.alf@example.net','$2y$10$JyF0EjeobhgLFf5RgE8z5ujLARR8T0ENxzW6/du1g7zdlBIXViMrO','88Gn1IX5fu','2017-03-06 21:28:47','2017-03-06 21:28:47'),(8,'Theresa Hansen','otorphy@example.org','$2y$10$rnXvHVux/XPRiCQ6T7aG4eg3xOcw.ppu8bWR3pQPbYqQ3qnFB1Mdy','vjHMzrbIVf','2017-03-06 21:28:47','2017-03-06 21:28:47'),(9,'Mr. Gaston Morissette','wilhelmine.dickens@example.org','$2y$10$F6QtXe4lpSQOrdEoHxy8LO3TbgxVZlzxHSDFvSlnWLpfDL6pw3l0C','QcIXlVWAuF','2017-03-06 21:28:47','2017-03-06 21:28:47'),(10,'Mr. Nathan Nicolas Jr.','lavon.ward@example.org','$2y$10$w6vAMSVAvekJ6Yd9fjwPbuDRrpvIfR2zgAR/uCayN1mS1UTZ1QQbm','kyBqaAVqZg','2017-03-06 21:28:47','2017-03-06 21:28:47'),(11,'Alvis O\'Conner','zack.ferry@example.com','$2y$10$cIDg0sYVTT3NvxXEvf7FGeC8upgJnjGtlLrqby7OMjr4Up3QZMbAq','bLzQg2aD4x','2017-03-06 21:28:47','2017-03-06 21:28:47'),(12,'Dr. Randal Schoen','ikoelpin@example.org','$2y$10$hUxeEGptsLG2sVXwSiQtjeT92aQBJ.XbLZxULAPRniAhr/x9ZI0xy','O5EcSyA8G8','2017-03-06 21:28:47','2017-03-06 21:28:47'),(13,'Una Upton','alfonso.hickle@example.com','$2y$10$MB6FFq1jHOM/hsX.eGzJzuGcU24MxwuOqYHwzOLE4cU1kpowl4nqO','5pft0apusZ','2017-03-06 21:28:47','2017-03-06 21:28:47'),(14,'Prof. Kylie Thompson','lbeer@example.com','$2y$10$vZdVUm0hryGQdNCPTjtTdemaxZtYUFYwtkQSJCt4oAM1kaMqUDBbC','RztzX2kac3','2017-03-06 21:28:47','2017-03-06 21:28:47'),(15,'Miss Jackie Blanda','jon90@example.org','$2y$10$dUJZV4UA1FUbdymOPS1Oh.ah/dsLcp5vAXnGBRRlAEsxst0wVXjjC','1MUCPCXSHo','2017-03-06 21:28:47','2017-03-06 21:28:47'),(16,'Prof. Jaylan Roberts','aniyah.cronin@example.org','$2y$10$hEq3r5U3Ad20pxRDfxyWueRJXztcb5pGs2uiiv4p8Up6FvCgVPPKq','IpPh7mEtpd','2017-03-06 21:28:47','2017-03-06 21:28:47'),(17,'Prof. Kailey Wuckert','romaguera.gabriella@example.net','$2y$10$NZCq0wXXStHDViUofEjGPOaUYjRpXn99fK1yqVBKslcyLfTCDyPoy','u5UCweMoC0','2017-03-06 21:28:47','2017-03-06 21:28:47'),(18,'Mr. Dominic Becker','kfay@example.net','$2y$10$CjtV3yKAUesEOYSpatQVh.613j4kaBkZidT/akW12s7qNquZTQ6qK','2j1uS3ivid','2017-03-06 21:28:47','2017-03-06 21:28:47'),(19,'Boyd Fadel','lelia.bogan@example.com','$2y$10$Q7WKMMn70GToacN43ZiDDuxa7o98KRJtK.aioWllrIlRno.FZy.VW','oeEqbr4Ynk','2017-03-06 21:28:47','2017-03-06 21:28:47'),(20,'Laurence Carter','emmerich.harvey@example.org','$2y$10$mOzL4E9Z7fceENLQHODYQuUlrtiYagM/HZxKuI0k/91NrylORkos6','N0vE3EDSl4','2017-03-06 21:28:47','2017-03-06 21:28:47'),(21,'Prof. Delilah Boehm DVM','schulist.hipolito@example.net','$2y$10$7TZ3QULJ2WfEtudl3vhquuxc7NlS5cTW.V3NeWgAfIfskBHDBFCyy','0fHANKgB9M','2017-03-06 21:28:47','2017-03-06 21:28:47'),(22,'Ewell Heathcote','amy.stiedemann@example.org','$2y$10$OsReYbN7WIS5o40aDgbeWOncBRs/9NsOZYcioYkVwm5WFih699fH6','E9C9fSrcQ3','2017-03-06 21:28:47','2017-03-06 21:28:47'),(23,'Dr. Kyler Littel Sr.','jazmyne.lowe@example.org','$2y$10$fyNgjXjoDrtn8n4cefbZtuuei.dJgjmaNeZdqmt69zLhYRVV19VPS','PBbBU5dsNF','2017-03-06 21:28:47','2017-03-06 21:28:47'),(24,'Ms. Andreanne Gerlach Sr.','dbechtelar@example.com','$2y$10$JACceHYwda9TL6zy.lDhAesHZBPeHpwW5trUTtlBR1FvxqzzZ4BBO','bIbmNzxVrW','2017-03-06 21:28:47','2017-03-06 21:28:47'),(25,'Berry Dickinson III','terry.omer@example.org','$2y$10$1h5LgfJsTmOw.wPkfqIJNeGfotQR/tsQOKKZINWxGKfNuaVlTeNZu','pfvhRMbwYg','2017-03-06 21:28:47','2017-03-06 21:28:47'),(26,'Kay Abernathy','nikolaus.kristopher@example.org','$2y$10$IdPHjiE0SCwypS3ki/txtuxIToRDGyqtkFqliAYouIObiT.KIUGd.','V3IxlwdREz','2017-03-06 21:28:47','2017-03-06 21:28:47'),(27,'Dr. Alexandra Raynor Sr.','mckenna69@example.net','$2y$10$2A/S6MaRWPaNk47bJJOcfORklYAurMGxDcA12qH5Mz2Afejhaqw4W','9vznhf6TlA','2017-03-06 21:28:47','2017-03-06 21:28:47'),(28,'Lorenza Bruen','owehner@example.org','$2y$10$QmbGtxeixiCLt9qm.PVUC.vzD9EQTMZqAwut69hJGDZ6D6iK911B6','Frjav8dYc6','2017-03-06 21:28:47','2017-03-06 21:28:47'),(29,'Brandon Kassulke I','daisy07@example.net','$2y$10$wBxafQiqsVZr2FM7f1TS3uIw.DX.zMO5bP8hpi7mBpxE1XJrGC7Fu','lsXGadUNVk','2017-03-06 21:28:47','2017-03-06 21:28:47'),(30,'Mrs. Ozella Rohan','dahlia.heaney@example.net','$2y$10$XyjbVoGktGx6idkdDlcm7.Q3XgoHikbBo2e8XCOyzDe21wF3bxgHK','ZlQCJuMtw8','2017-03-06 21:28:47','2017-03-06 21:28:47'),(31,'Ibrahim Zemlak','rmitchell@example.org','$2y$10$VlKDm49VyxviONMxrwpyeuL429vY/06b/m/fzq.6Gp2BLK2x7oz7S','b4s95MhNMu','2017-03-06 21:28:47','2017-03-06 21:28:47'),(32,'Dr. Helene Paucek','clemke@example.com','$2y$10$iNA85jTvMcK3JjBvbY4J5OwUvNOo8jccynsA/RTkPxyktf1o59VRW','TQkAp5vw4s','2017-03-06 21:28:47','2017-03-06 21:28:47'),(33,'Viola Gottlieb','lind.ford@example.org','$2y$10$z95VUte5DHdv74kthCRiDufBlbZAnOOBHDW0O9hibYjBK2wPIQRAu','V9fgq1MR2p','2017-03-06 21:28:47','2017-03-06 21:28:47'),(34,'Chesley Kuhn','jeramy24@example.com','$2y$10$VyKKtDnDBnyLdogVsvK./.f7eUylvHlTzzRvG0UIki2/1iP9iY0ha','cZp11A1utg','2017-03-06 21:28:48','2017-03-06 21:28:48'),(35,'Ralph Dicki','stanton28@example.net','$2y$10$cFPQrPHp5k/gieWSPJTjfeT3F7ORAz3wFU898XOzGoGZu588P0mT.','HtLS9fhphM','2017-03-06 21:28:48','2017-03-06 21:28:48'),(36,'Bret Senger II','fzulauf@example.net','$2y$10$hUi3RzvsWrfYB.lIlDr0IObXnlYrH5pF1Q0buemCeR0QooNRt.y2K','52twOPjdao','2017-03-06 21:28:48','2017-03-06 21:28:48'),(37,'Murphy Corkery','frieda09@example.com','$2y$10$JfMVvxO30UqT15rzztF8P.I/PGT1eEvlOawOA4SDUnvZ0i9LIfSn2','VNSZZHfO27','2017-03-06 21:28:48','2017-03-06 21:28:48'),(38,'Carissa Kozey','beier.annetta@example.net','$2y$10$jKcPPV2m0/UJ2lXAYJjthepTEULxVHMLNx2HBTT09qjLyhxOyLp0W','0cFjsryhun','2017-03-06 21:28:48','2017-03-06 21:28:48'),(39,'Prof. Berry Kerluke III','ywalsh@example.net','$2y$10$Meo6Yq8z0I/jvB0CFTJKte/6Ca8Slv7jzgF/TFwoFNa1N4drtryHi','f5aDlw90YU','2017-03-06 21:28:48','2017-03-06 21:28:48'),(40,'Lavada Russel','murl28@example.org','$2y$10$sEUrMTgPo0bf2cdGqa77ee5gCwkLawoO2t3h2VvJo9SEe4.m1ZNYS','BIXwpS2LO6','2017-03-06 21:28:48','2017-03-06 21:28:48'),(41,'Rod Gislason','jalyn90@example.org','$2y$10$anOCJ2ec8wc92MLXzK59huAF6qPkJc5R81g2C.I0KiE3olnkZ.WOi','ktYelzlJ6P','2017-03-06 21:28:48','2017-03-06 21:28:48'),(42,'Guy Kreiger','schamberger.elza@example.org','$2y$10$RJsX1HurTSNMT8McV5aLxeFIQ7Ptgg5924BtLdU9k6TFhif/KLgN2','sDFxmnBV5N','2017-03-06 21:28:48','2017-03-06 21:28:48'),(43,'Estefania Kuphal','reta.kuvalis@example.com','$2y$10$V3qGP08Vd5Tj/WnE34toy.AuIE8CU87UmRuWkmM8ebBQgzKZ/ch3C','7nSvuzfMxg','2017-03-06 21:28:48','2017-03-06 21:28:48'),(44,'Eve Goyette','chauncey.goyette@example.com','$2y$10$6eNgZwrB1BPhNmfiYnnF4eNbvpQtnyCvv1gHtaCkaFQh9XEkgWV9u','gOo1eWZvRl','2017-03-06 21:28:48','2017-03-06 21:28:48'),(45,'Prof. Bernita O\'Connell V','jaleel.gaylord@example.org','$2y$10$nRuPB2L0KBAL38.lTWv3Ge4b0Hl7JD24VhOkZloUrphmt/0x4IgrS','RY21nDEkHd','2017-03-06 21:28:48','2017-03-06 21:28:48'),(46,'Maryse Frami II','serenity.ward@example.com','$2y$10$UgNSIYRByQTFaOa8ei2nDOF0r2bVIzH/aT8X1qscI7wGSAzm47816','14RtbUNh9q','2017-03-06 21:28:48','2017-03-06 21:28:48'),(47,'Wilson Blanda','lucile.sporer@example.com','$2y$10$VAfm0mtUt98ZTkjqZLSs3uOHMYrgU9ZNnZQU/qviHa2MwNUNdPplu','aib9sR4NTJ','2017-03-06 21:28:48','2017-03-06 21:28:48'),(48,'Dr. Megane Nitzsche DDS','vkirlin@example.net','$2y$10$I4H2Wdb3M.3f0sQqg0oGAO//znKBiVPT.OifyOzXnlNtgBKztzEp6','CdelECVZOy','2017-03-06 21:28:48','2017-03-06 21:28:48'),(49,'Nicola Greenholt','demetrius.aufderhar@example.com','$2y$10$s/UKO.DYja4B28BQo2lt9OSOBOKS9284rui3l5U5NJTYEX7EgBXQ.','xbjJwviRUW','2017-03-06 21:28:48','2017-03-06 21:28:48'),(50,'Birdie Douglas','huel.jaeden@example.com','$2y$10$vr4p71SVIjAbT9b3XuVb4uyVM0tNFfspU2Ju3RSDbuBUDS1xwC9gW','j4Yfy4BCV7','2017-03-06 21:28:48','2017-03-06 21:28:48'),(51,'Tressa Trantow','delbert27@example.net','$2y$10$V56Sil8rEwxskpXxHickV.x5m5XPkY/EOd2mkaMjoVSlaz23pOJ56','BXnBrsM9Fj','2017-03-06 21:28:48','2017-03-06 21:28:48');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-03-06 21:43:21
